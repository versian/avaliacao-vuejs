import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/home/home.template.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/*',
      name: 'Home',
      //component: Home
      component: () => import('../pages/home/home.template.vue')
    }
  ]
})

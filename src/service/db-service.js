import PlanetService from './planet-service';

class DbService {

    static async _initDb() {

        var docs = await PlanetService._db.allDocs();

        if(docs.total_rows > 0)
            return;

        var planets = [];

        $.ajax({
            type: "GET",
            url: 'https://swapi.co/api/planets/?format=json',
            async: false,
            dataType: "json",
            success: (data) => {
                planets = data.results;
            },
            error: function (error) {
                console.log(error);
            }
        });

        planets.forEach(planet => {
            planet.residents = this._getResidentsByPlanet(planet);
            PlanetService.savePlanet(planet);
        });
    }

    static _getResidentsByPlanet(planet) {

        var residents = planet.residents.map(residentApi => {
            let resident = this._getResidentById(residentApi);
            return resident;
        });

        return residents;
    }

    static _getResidentById(url) {

        var resident = {};

        $.ajax({
            type: "GET",
            url: url,
            async: false,
            dataType: "json",
            success: (data) => {
                resident = data;
            },
            error: function (error) {
                console.log(error);
            }
        });

        return resident;
    }
}

export default DbService;
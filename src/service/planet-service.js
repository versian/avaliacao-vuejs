import PouchDb from 'pouchdb-browser';

class PlanetService {

    static _db = new PouchDb('planets');

    // Get Planets
    static getPlanets(id) {
        let result;

        if (id)
            result = this._db.get(id);
        else
            result = this._db.allDocs({include_docs: true}).then(result => { return result.rows.map(value => { return value.doc }) });

        return result;
    }

    // Craete/Update
    static savePlanet(planet) {
        let result;

        if (planet) {

            if(!planet._id)
                planet._id = "id_" + Math.random();

            result = this._db.put(planet);
        }

        return result;
    }

    // Delete
    static deletePlanet(planet) {
        let result;

        if (planet)
            result = this._db.remove(planet);

        return result;
    }

}

export default PlanetService;
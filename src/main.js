// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App'
import router from './router'
import VueGoodTablePlugin from 'vue-good-table';
import Notifications from 'vue-notification'
import VueLodash from 'vue-lodash'  

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faPlusSquare, faPlusCircle, faCheck, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-good-table/dist/vue-good-table.css'

Vue.use(BootstrapVue)
Vue.use(VueGoodTablePlugin)
Vue.use(Notifications)
Vue.use(VueLodash)
Vue.config.productionTip = false
Vue.config.productionTip = false
Vue.config.devtools = true
Vue.config.performance = true
library.add(faCheck)

Vue.component('font-awesome-icon', FontAwesomeIcon)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
